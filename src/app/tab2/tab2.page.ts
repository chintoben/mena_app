import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { HTTP } from '@ionic-native/http/ngx';


@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {

  dataitem: any;

  constructor(private http: HttpClient) {
 
  }

  ionViewWillEnter() {


    
    this.getpatient().subscribe(res => {
      if(res != null){
        this.dataitem = res;
        console.log("this.dataitem",this.dataitem);
      }

    });

}

  // async patient_all() {
  //   this.getpatient().subscribe(res => {
  //     console.log(res);

  //   });
  // }
  

  getpatient() {
    return this.http.get("https://app.mth.go.th/mena_api/?apikey=ha2020&a=adm&action=item_all");
   }

}
