import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-drug-allergy',
  templateUrl: './drug-allergy.page.html',
  styleUrls: ['./drug-allergy.page.scss'],
})
export class DrugAllergyPage implements OnInit {
  patientid: any;
  patientname: any;
  constructor(
    private activatedRoute: ActivatedRoute,
  ) { 
    this.patientname = this.activatedRoute.snapshot.paramMap.get('patientname');

  }

  ionViewWillEnter() {
    alert(this.patientname);
 }
  ngOnInit() {
    
  }

}
