import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DrugAllergyPage } from './drug-allergy.page';

describe('DrugAllergyPage', () => {
  let component: DrugAllergyPage;
  let fixture: ComponentFixture<DrugAllergyPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DrugAllergyPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DrugAllergyPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
