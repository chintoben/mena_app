import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { DrugAllergyPageRoutingModule } from './drug-allergy-routing.module';
import { DrugAllergyPage } from './drug-allergy.page';

import { RouterModule } from '@angular/router';
import { ExploreContainerComponentModule } from '../explore-container/explore-container.module';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ExploreContainerComponentModule,
    DrugAllergyPageRoutingModule,
    RouterModule.forChild([{ path: '', component: DrugAllergyPage }])
  ],
  declarations: [DrugAllergyPage]
})
export class DrugAllergyPageModule {}



