import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AssitantPage } from './assitant.page';

const routes: Routes = [
  {
    path: '',
    component: AssitantPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AssitantPageRoutingModule {}
