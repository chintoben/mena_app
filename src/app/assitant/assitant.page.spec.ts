import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AssitantPage } from './assitant.page';

describe('AssitantPage', () => {
  let component: AssitantPage;
  let fixture: ComponentFixture<AssitantPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssitantPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AssitantPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
