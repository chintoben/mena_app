import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AssitantPageRoutingModule } from './assitant-routing.module';

import { AssitantPage } from './assitant.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AssitantPageRoutingModule
  ],
  declarations: [AssitantPage]
})
export class AssitantPageModule {}
