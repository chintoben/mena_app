import { PublishService } from './../services/publish.service';
import { UserService } from './../storage/user.service';
import { ApiUserService } from './../services/api-user.service';

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoadingController } from '@ionic/angular';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { HttpClientModule } from '@angular/common/http';
import { HTTP } from '@ionic-native/http/ngx';
import { $ } from 'protractor';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],

})



export class LoginPage implements OnInit {
  username: string = "";
  password: string = "";
  constructor(private http: HttpClient ,
    private router: Router,
     //private userservice: ApiUserService,
    // private userstorage: UserService,
     //public loadingController: LoadingController,
     private publish: PublishService
    ) {   }

  ngOnInit() {
  }


  async login() {
    if (this.username === '' || this.password === '') {
      document.getElementById('lb_blank').hidden = false;
      document.getElementById('lb_incorrect').hidden = true;
    return;

  }
  if (this.username != "vdev") {
    document.getElementById('lb_blank').hidden = true;
    document.getElementById('lb_incorrect').hidden = false;
  return;
  }

  this.getLogin(this.username, this.password).subscribe(res => {
    console.log("res",res);



    if (res == 400) {
      document.getElementById('lb_blank').hidden = true;
      document.getElementById('lb_incorrect').hidden = false;
      console.log("login" , res);
    return;
    }


    document.getElementById('lb_blank').hidden = true;
    document.getElementById('lb_incorrect').hidden = true;
    this.router.navigateByUrl('tabs/tab1');

  });


  


  }
  


  getLogin(username, password) {
   return this.http.get("https://app.mth.go.th/mena_api/?apikey=ha2020&a=user&action=login&username="+ username +"&password="+ password);

  }

}






// export class LoginPage implements OnInit {

//   constructor(private router: Router,
//     private userservice: ApiUserService,
//     private userstorage: UserService,
//     public loadingController: LoadingController,
//     private publish: PublishService
//   ) { }

//   ngOnInit() {
//   }

 
// }





