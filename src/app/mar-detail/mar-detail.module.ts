import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MarDetailPageRoutingModule } from './mar-detail-routing.module';

import { MarDetailPage } from './mar-detail.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MarDetailPageRoutingModule
  ],
  declarations: [MarDetailPage]
})
export class MarDetailPageModule {}
