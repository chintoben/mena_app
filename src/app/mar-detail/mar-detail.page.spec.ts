import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MarDetailPage } from './mar-detail.page';

describe('MarDetailPage', () => {
  let component: MarDetailPage;
  let fixture: ComponentFixture<MarDetailPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MarDetailPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MarDetailPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
