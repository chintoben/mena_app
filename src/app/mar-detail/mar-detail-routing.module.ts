import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MarDetailPage } from './mar-detail.page';

const routes: Routes = [
  {
    path: '',
    component: MarDetailPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MarDetailPageRoutingModule {}
