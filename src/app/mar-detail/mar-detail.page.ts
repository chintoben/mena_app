import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-mar-detail',
  templateUrl: './mar-detail.page.html',
  styleUrls: ['./mar-detail.page.scss'],
})
export class MarDetailPage implements OnInit {

  patientid: any;
  patientname: any;
  constructor(
    private activatedRoute: ActivatedRoute,

  ) {
    this.patientname = this.activatedRoute.snapshot.paramMap.get('patientname');

   }

   ionViewWillEnter() {
      alert(this.patientname);
   }

   
  ngOnInit() {
  }

}
