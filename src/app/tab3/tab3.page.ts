import { Component } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { HTTP } from '@ionic-native/http/ngx';
import { Storage } from '@ionic/storage';
import * as moment from 'moment';



@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page {
  _date: string = moment().format('DD/MM/YYYY');
  _time: string = moment().format('hh:mm');
  constructor(
    private http: HttpClient ,
    private storage: Storage
  ) {


  }





  ionViewWillEnter() {
 

    // let all_stat = 21 ;
    // let success_stat = 21 ;
    // let all_prn = 11 ;
    // let success_prn  = 21 ;
    // let all_specialroute = 7 ;
    // let success_specialroute  = 21 ;
    // let all_infusion = 13 ;
    // let success_infusion  = 21 ;




    // console.log("Datenow ",this._date);
    // this.now_date = this._date ;
    // this.now_time = this._time ;

    // this.getallstat().subscribe(res => {
    //   if(!res){
    //     this.all_stat = 0 ;
    //   }else{
    //     this.all_stat = JSON.parse(JSON.stringify(res)).length ;
    //   }
    // });
    //  this.getallprn().subscribe(res => {
    //   if(!res){
    //     this.all_prn = 0 ;
    //   }else{
    //     this.all_prn = JSON.parse(JSON.stringify(res)).length ;
    //   }
    //  });
    //  this.getallinfusion().subscribe(res => {
    //   if(!res){
    //     this.all_infusion = 0 ;
    //   }else{
    //     this.all_infusion = JSON.parse(JSON.stringify(res)).length ;
    //   }
    // });
    // this.getallspecialroute().subscribe(res => {
    //   if(!res){
    //     this.all_specialroute = 0 ;
    //   }else{
    //     this.all_specialroute = JSON.parse(JSON.stringify(res)).length ;
    //   }
    // });
   
    // this.getsuccess_stat().subscribe(res => {
    //   console.log(res);
    //   if(!res){
    //     this.success_stat = 0 ;
    //   }else{
    //     this.success_stat = JSON.parse(JSON.stringify(res)).length ;
    //   }
    // });
   

    // this.getsuccess_prn().subscribe(res => {
    //   if(!res){
    //     this.success_prn = 0 ;
    //   }else{
    //     this.success_prn = JSON.parse(JSON.stringify(res)).length ;
    //   }
    // });

    // this.getsuccess_specialroute().subscribe(res => {
    //   if(!res){
    //     this.success_specialroute = 0 ;
    //   }else{
    //     this.success_specialroute = JSON.parse(JSON.stringify(res)).length ;
    //   }
    // });
    // this.getsuccess_infusion().subscribe(res => {
    //   if(!res){
    //     this.success_infusion = 0 ;
    //   }else{
    //     this.success_infusion = JSON.parse(JSON.stringify(res)).length ;
    //   }
    // });

    // this.getall_patient().subscribe(res => {
    //   if(!res){
    //     this.all_patient = 0 ;
    //   }else{
    //     this.all_patient = JSON.parse(JSON.stringify(res)).length ;
    //   }
    // });


  }


  getallstat() {
    return this.http.get("https://app.mth.go.th/mena_api/?apikey=ha2020&a=medplan&action=stat");
   }
   getallprn() {
    return this.http.get("https://app.mth.go.th/mena_api/?apikey=ha2020&a=medplan&action=prn");
   }
   getallinfusion() {
    return this.http.get("https://app.mth.go.th/mena_api/?apikey=ha2020&a=medplan&action=infusion");
   }
   getallspecialroute() {
    return this.http.get("https://app.mth.go.th/mena_api/?apikey=ha2020&a=medplan&action=specialroute");
   }
   
   getsuccess_stat() {
    return this.http.get("https://app.mth.go.th/mena_api/?apikey=ha2020&a=medplan&action=stat&pay=y");
   }
   getsuccess_prn() {
    return this.http.get("https://app.mth.go.th/mena_api/?apikey=ha2020&a=medplan&action=prn&pay=y");
   }
   getsuccess_specialroute() {
    return this.http.get("https://app.mth.go.th/mena_api/?apikey=ha2020&a=medplan&action=specialroute&pay=y");
   }
   getsuccess_infusion() {
    return this.http.get("https://app.mth.go.th/mena_api/?apikey=ha2020&a=medplan&action=infusion&pay=y");
   }

   getall_patient() {
    return this.http.get("https://app.mth.go.th/mena_api/?apikey=ha2020&a=adm&action=item_all");
   }

}
