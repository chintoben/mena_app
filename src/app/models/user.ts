export class User {
    login_id: number;
    username: string;
    userrule: string;
    title: string;
    fname: string;
    lname: string;
    group_id: string;
    loginstatus: number;
    sessioncode: string;
    lastlogindatetime: Date;
    mobile: string;
    email: string;
    picture: string;
    description: string;
    address1: string;
    address2: string;
}
