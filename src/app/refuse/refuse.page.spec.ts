import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { RefusePage } from './refuse.page';

describe('RefusePage', () => {
  let component: RefusePage;
  let fixture: ComponentFixture<RefusePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RefusePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(RefusePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
