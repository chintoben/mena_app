import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RefusePageRoutingModule } from './refuse-routing.module';

import { RefusePage } from './refuse.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RefusePageRoutingModule
  ],
  declarations: [RefusePage]
})
export class RefusePageModule {}
