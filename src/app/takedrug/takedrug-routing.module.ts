import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TakedrugPage } from './takedrug.page';

const routes: Routes = [
  {
    path: '',
    component: TakedrugPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TakedrugPageRoutingModule {}
