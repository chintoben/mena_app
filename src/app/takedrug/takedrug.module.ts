import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TakedrugPageRoutingModule } from './takedrug-routing.module';

import { TakedrugPage } from './takedrug.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TakedrugPageRoutingModule
  ],
  declarations: [TakedrugPage]
})
export class TakedrugPageModule {}
