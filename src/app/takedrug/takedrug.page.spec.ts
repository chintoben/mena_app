import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { TakedrugPage } from './takedrug.page';

describe('TakedrugPage', () => {
  let component: TakedrugPage;
  let fixture: ComponentFixture<TakedrugPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TakedrugPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(TakedrugPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
