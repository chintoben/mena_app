import { Component } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { HTTP } from '@ionic-native/http/ngx';
import { Storage } from '@ionic/storage';
import * as moment from 'moment';



@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {
  _date: string = moment().format('DD/MM/YYYY');
  _time: string = moment().format('hh:mm');
  constructor(
    private http: HttpClient ,
    private storage: Storage
    
    ) {


  }
  ngOnInit() {

    
  }
 
  ionViewWillEnter() {
 

    // let shift_n1 = 21 ;
    // let shift_n2 = 11 ;
    // let shift_m1 = 13 ;
    // let shift_m2 = 7 ;
    // let shift_a1 = 19 ;
    // let shift_a2 = 10 ;



    // console.log("Datenow ",this._date);
    // this.now_date = this._date ;
    // this.now_time = this._time ;

    // this.getallshiftnight().subscribe(res => {
    //   this.shift_n1 = JSON.parse(JSON.stringify(res)).length ;
    //   for (var i = 0; i < JSON.parse(JSON.stringify(res)).length; i++) {

    //   }
    // });

    //  this.getallshiftmorning().subscribe(res => {
    //   this.shift_m1 = JSON.parse(JSON.stringify(res)).length ;
    //  });

    //  this.getallshiftafternoon().subscribe(res => {
    //   this.shift_a1 = JSON.parse(JSON.stringify(res)).length ;
    // });

    // this.getshiftnight_success().subscribe(res => {
    //   if(!res){
    //     this.shift_n2 = 0 ;
    //   }else{
    //     this.shift_n2 = JSON.parse(JSON.stringify(res)).length ;
    //   }

    // });
   
    // this.getshiftmorning_success().subscribe(res => {
    //   console.log(res);
    //   if(!res){
    //     this.shift_m2 = 0 ;
    //   }else{
    //     this.shift_m2 = JSON.parse(JSON.stringify(res)).length ;
    //   }
    // });
   

    // this.getshiftafternoon_success().subscribe(res => {
    //   if(!res){
    //     this.shift_a2 = 0 ;
    //   }else{
    //     this.shift_a2 = JSON.parse(JSON.stringify(res)).length ;
    //   }
    // });

    // this.getall_patient().subscribe(res => {
    //   if(!res){
    //     this.all_patient = 0 ;
    //   }else{
    //     this.all_patient = JSON.parse(JSON.stringify(res)).length ;
    //   }
    // });


  }
 



   get_datenow() {
    return ;
   }

  getallshiftnight() {
    return this.http.get("https://app.mth.go.th/mena_api/?apikey=ha2020&a=medplan&action=medshift&shift=1");
   }
   getallshiftmorning() {
    return this.http.get("https://app.mth.go.th/mena_api/?apikey=ha2020&a=medplan&action=medshift&shift=2");
   }
   getallshiftafternoon() {
    return this.http.get("https://app.mth.go.th/mena_api/?apikey=ha2020&a=medplan&action=medshift&shift=3");
   }
   getshiftnight_success() {
    return this.http.get("https://app.mth.go.th/mena_api/?apikey=ha2020&a=medplan&action=medshift&shift=1&pay=Y");
   }
   getshiftmorning_success() {
    return this.http.get("https://app.mth.go.th/mena_api/?apikey=ha2020&a=medplan&action=medshift&shift=2&pay=Y");
   }
   getshiftafternoon_success() {
    return this.http.get("https://app.mth.go.th/mena_api/?apikey=ha2020&a=medplan&action=medshift&shift=3&pay=Y");
   }

   getall_patient() {
    return this.http.get("https://app.mth.go.th/mena_api/?apikey=ha2020&a=adm&action=item_all");
   }


}
