import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { HolddrugPage } from './holddrug.page';

describe('HolddrugPage', () => {
  let component: HolddrugPage;
  let fixture: ComponentFixture<HolddrugPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HolddrugPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(HolddrugPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
