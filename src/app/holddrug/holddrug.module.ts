import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { HolddrugPageRoutingModule } from './holddrug-routing.module';

import { HolddrugPage } from './holddrug.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HolddrugPageRoutingModule
  ],
  declarations: [HolddrugPage]
})
export class HolddrugPageModule {}
