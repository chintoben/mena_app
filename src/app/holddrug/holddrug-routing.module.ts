import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HolddrugPage } from './holddrug.page';

const routes: Routes = [
  {
    path: '',
    component: HolddrugPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HolddrugPageRoutingModule {}
