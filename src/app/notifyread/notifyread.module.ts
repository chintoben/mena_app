import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { NotifyreadPageRoutingModule } from './notifyread-routing.module';

import { NotifyreadPage } from './notifyread.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NotifyreadPageRoutingModule
  ],
  declarations: [NotifyreadPage]
})
export class NotifyreadPageModule {}
