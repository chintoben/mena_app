import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { NotifyreadPage } from './notifyread.page';

describe('NotifyreadPage', () => {
  let component: NotifyreadPage;
  let fixture: ComponentFixture<NotifyreadPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NotifyreadPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(NotifyreadPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
