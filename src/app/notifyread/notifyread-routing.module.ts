import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NotifyreadPage } from './notifyread.page';

const routes: Routes = [
  {
    path: '',
    component: NotifyreadPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NotifyreadPageRoutingModule {}
