import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { HTTP } from '@ionic-native/http/ngx';
import { Storage } from '@ionic/storage';
import * as moment from 'moment';
@Component({
  selector: 'app-report',
  templateUrl: './report.page.html',
  styleUrls: ['./report.page.scss'],
})
export class ReportPage implements OnInit {
  _date: string = moment().format('DD/MM/YYYY');
  _time: string = moment().format('hh:mm');
  constructor(
    private http: HttpClient ,
    private storage: Storage
  ) { }

  ngOnInit() {
  }



  ionViewWillEnter() {


    // console.log("Datenow ",this._date);
    // this.now_date = this._date ;
    // this.now_time = this._time ;
    // this.shift_n2  = 0 ;
    // this.shift_m2  = 0 ;
    // this.shift_a2  = 0 ;
    // this.error_stat = 0 ;
    // this.error_prn  = 0 ;
    // this.error_specialroute  = 0 ;
    // this.error_infusion  = 0 ;

    // this.getshiftnight_success().subscribe(res => {
    //   if(!res){
    //     this.shift_n1 = 0 ;
    //   }else{
    //     this.shift_n1= JSON.parse(JSON.stringify(res)).length ;
    //   }

    // });
   
    // this.getshiftmorning_success().subscribe(res => {
    //   console.log(res);
    //   if(!res){
    //     this.shift_m1 = 0 ;
    //   }else{
    //     this.shift_m1 = JSON.parse(JSON.stringify(res)).length ;
    //   }
    // });
   

    // this.getshiftafternoon_success().subscribe(res => {
    //   if(!res){
    //     this.shift_a1 = 0 ;
    //   }else{
    //     this.shift_a1 = JSON.parse(JSON.stringify(res)).length ;
    //   }
    // });



    // this.getsuccess_stat().subscribe(res => {
    //   console.log(res);
    //   if(!res){
    //     this.success_stat = 0 ;
    //   }else{
    //     this.success_stat = JSON.parse(JSON.stringify(res)).length ;
    //   }
    // });
   

    // this.getsuccess_prn().subscribe(res => {
    //   if(!res){
    //     this.success_prn = 0 ;
    //   }else{
    //     this.success_prn = JSON.parse(JSON.stringify(res)).length ;
    //   }
    // });

    // this.getsuccess_specialroute().subscribe(res => {
    //   if(!res){
    //     this.success_specialroute = 0 ;
    //   }else{
    //     this.success_specialroute = JSON.parse(JSON.stringify(res)).length ;
    //   }
    // });
    // this.getsuccess_infusion().subscribe(res => {
    //   if(!res){
    //     this.success_infusion = 0 ;
    //   }else{
    //     this.success_infusion = JSON.parse(JSON.stringify(res)).length ;
    //   }
    // });



  }
 


  getshiftnight_success() {
    return this.http.get("https://app.mth.go.th/mena_api/?apikey=ha2020&a=medplan&action=medshift&shift=1&pay=Y");
   }
   getshiftmorning_success() {
    return this.http.get("https://app.mth.go.th/mena_api/?apikey=ha2020&a=medplan&action=medshift&shift=2&pay=Y");
   }
   getshiftafternoon_success() {
    return this.http.get("https://app.mth.go.th/mena_api/?apikey=ha2020&a=medplan&action=medshift&shift=3&pay=Y");
   }

   getsuccess_stat() {
    return this.http.get("https://app.mth.go.th/mena_api/?apikey=ha2020&a=medplan&action=stat&pay=y");
   }
   getsuccess_prn() {
    return this.http.get("https://app.mth.go.th/mena_api/?apikey=ha2020&a=medplan&action=prn&pay=y");
   }
   getsuccess_specialroute() {
    return this.http.get("https://app.mth.go.th/mena_api/?apikey=ha2020&a=medplan&action=specialroute&pay=y");
   }
   getsuccess_infusion() {
    return this.http.get("https://app.mth.go.th/mena_api/?apikey=ha2020&a=medplan&action=infusion&pay=y");
   }



}
