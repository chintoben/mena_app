
import { User } from './../models/user';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
// import * as moment from 'moment';
import { Storage } from '@ionic/storage';
import { UserService } from '../storage/user.service';

@Injectable({
  providedIn: 'root'
})
export class ApiUserService {
  base_url = "https://app.mth.go.th/mena_api/?a=user&action=login&username=vdev&password=ha2020";

  constructor(private http: HttpClient,
    private userstorage: UserService) { }

    httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    }

    handleError(error: HttpErrorResponse) {
      if (error.error instanceof ErrorEvent) {
        console.error('An error occurred:', error.error.message);
      } else {
        console.error(
          'Backend returned code ${error.status}, ' +
          'body was: ${error.error}');
      }
      return throwError(
        'Something bad happened; please try again later.');
    };

    getLogin(username, password) {
      return this.http.get("https://app.mth.go.th/mena_api/?a=user&action=login&username="+ username +"&password="+ password).pipe(
        retry(2),
        catchError(this.handleError)
      );
    }
    getLogin2(username, password) {
      return this.http.get("https://app.mth.go.th/mena_api/?a=user&action=login&username="+ username +"&password="+ password);
    }

}
