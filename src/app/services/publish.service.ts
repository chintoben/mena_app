import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PublishService {

  private UserChange = new Subject<any>();

  constructor() { }

  PublishUserChange(data: any) {
    this.UserChange.next(data);
  }



}
