import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./tabs/tabs.module').then(m => m.TabsPageModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'drug-allergy',
    loadChildren: () => import('./drug-allergy/drug-allergy.module').then( m => m.DrugAllergyPageModule)
  },
  {
    path: 'mar-detail',
    loadChildren: () => import('./mar-detail/mar-detail.module').then( m => m.MarDetailPageModule)
  },
  {
    path: 'report',
    loadChildren: () => import('./report/report.module').then( m => m.ReportPageModule)
  },
  {
    path: 'assitant',
    loadChildren: () => import('./assitant/assitant.module').then( m => m.AssitantPageModule)
  },
  {
    path: 'holddrug',
    loadChildren: () => import('./holddrug/holddrug.module').then( m => m.HolddrugPageModule)
  },
  {
    path: 'refuse',
    loadChildren: () => import('./refuse/refuse.module').then( m => m.RefusePageModule)
  },
  {
    path: 'takedrug',
    loadChildren: () => import('./takedrug/takedrug.module').then( m => m.TakedrugPageModule)
  },
  {
    path: 'mysetting',
    loadChildren: () => import('./mysetting/mysetting.module').then( m => m.MysettingPageModule)
  },
  {
    path: 'profile',
    loadChildren: () => import('./profile/profile.module').then( m => m.ProfilePageModule)
  },
  {
    path: 'notify',
    loadChildren: () => import('./notify/notify.module').then( m => m.NotifyPageModule)
  },
  {
    path: 'notifyread',
    loadChildren: () => import('./notifyread/notifyread.module').then( m => m.NotifyreadPageModule)
  }
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
